#!/usr/bin/env python3

import numpy as np
from math import pi
import matplotlib.pyplot as plt
import matplotlib.animation as anim
from matplotlib.widgets import Slider

def mcstep_lang(latt, lang_noise, dt):
    """Run one Monte Carlo step with Langevin dynamics

    (e.g. PRE 47 1525)

    """
    return latt \
        - dt * (lang_noise + \
        # 4 nearest neighbors N, S, E, W
        np.sin(latt - np.roll(latt, 1, axis=0)) + \
        np.sin(latt - np.roll(latt, -1, axis=0)) + \
        np.sin(latt - np.roll(latt, -1, axis=1)) + \
        np.sin(latt - np.roll(latt, 1, axis=1)) + \
        # 4 nearest neighbors NE, SE, SW, NW
        np.sin(latt - np.roll(latt, (1, -1), axis=(0, 1))) + \
        np.sin(latt - np.roll(latt, -1, axis=(0, 1))) + \
        np.sin(latt - np.roll(latt, (-1, 1), axis=(0, 1))) + \
        np.sin(latt - np.roll(latt, 1, axis=(0, 1))) )


def make_noise(noise, sz):
    """Lets make some (Langevin) noise"""
    return 2 * pi * noise * (np.random.rand(sz, sz) - 0.5)


def mcstep(latt, beta):
    """Compute the energy differences when doing a trial change of a spin, for all spins"""
    # Nearest neighbors N, S, E, W
    #latt = norm_angle(latt)
    sumold = np.cos(latt - np.roll(latt, 1, axis=0)) \
            + np.cos(latt - np.roll(latt, -1, axis=0)) \
            + np.cos(latt - np.roll(latt, -1, axis=1)) \
            + np.cos(latt - np.roll(latt, 1, axis=1))

    # New trial angles
    sz = latt.shape[0]
    new_theta = 2 * pi * np.random.rand(sz, sz)
    sumnew = np.cos(new_theta - np.roll(latt, 1, axis=0)) \
            + np.cos(new_theta - np.roll(latt, -1, axis=0)) \
            + np.cos(new_theta - np.roll(latt, -1, axis=1)) \
            + np.cos(new_theta - np.roll(latt, 1, axis=1))

    deltaE = - (sumnew - sumold)
    #print("deltaE<=0 count: ", np.sum(deltaE <= 0))

    # if deltaE <= 0, the flip is accepted
    latt = np.where(deltaE <= 0, new_theta, latt)

    # Acceptance prob
    a = np.minimum(1, np.exp(-beta * deltaE))

    r = np.random.rand(sz, sz)

    # Update those states where r <= a
    #print(np.sum(r<=a))
    return np.where(r <= a, new_theta, latt)

def norm_angle(angs):
    """Normalize angles to be between [-pi,pi]"""
    angs = angs % (2 * pi)
    return np.where(angs > pi, angs - 2 * pi, angs)
    #return angs

def winding_number(latt):
    """Find topological defects

    Calculate the winding numbers at each spin. See also Cauchy
    integral formula.

    """
    # Normalize angle so it's [-pi, pi]
    nlatt = norm_angle(latt.copy())

    # rectangle with origin site at top left corner
    S = np.roll(nlatt, -1, axis=0)
    SE = np.roll(nlatt, -1, axis=(0, 1))
    E = np.roll(nlatt, -1, axis=1)

    return norm_angle(S-nlatt) + norm_angle(SE-S) + norm_angle(E-SE) + norm_angle(nlatt-E)
#    return np.sin(np.roll(latt, 1, axis=(0, 1))) \
#        - np.cos(np.roll(latt, 1, axis=0)) \
#        - np.sin(latt) \
#        + np.cos(np.roll(latt, -1, axis=1))

    # E, N, W, S
#    return np.sin(np.roll(latt, -1, axis=1)) \
#        - np.cos(np.roll(latt, 1, axis=0)) \
#        - np.sin(np.roll(latt, 1, axis=1)) \
#        + np.cos(np.roll(latt, -1, axis=0))

#    return np.cos(np.roll(latt, -1, axis=1) - pi/2) \
#        + np.cos(np.roll(latt, 1, axis=0) - pi) \
#        + np.cos(np.roll(latt, 1, axis=1) - 3*pi/2) \
#        + np.cos(np.roll(latt, -1, axis=0) - 2*pi)

def plot_defects(latt, ax, ln):
    """Plot defects"""
    w = winding_number(latt)
    pos = np.argwhere(w > pi)
    neg = np.argwhere(w < -pi)
    print("Number of defects: ", np.sum(np.abs(w) >= pi))
    if ln is None:
        ln = ax.plot(pos[:,0], pos[:,1], 'r+')
        ln.append(ax.plot(neg[:,0], neg[:,1], 'b+')[0])
        return ln
    ln[0].set_data(pos[:,0]+0.5, pos[:,1]+0.5)
    ln[1].set_data(neg[:,0]+0.5, neg[:,1]+0.5)
    return ln

def change_temp(x):
    """Callback when temp was changed"""
    global beta
    beta = 1/max(x, 1e-5)

def doplot_start(latt):
    """Make a quiver plot of the lattice"""
    # matplotlib as quiver for this. But it wants separate U and V
    # arrays with x and y coordinates of the arrow vectors. So lets
    # convert our angles to x-y format
    u = np.cos(latt)
    v = np.sin(latt)
    fig, ax = plt.subplots()
    ln = plot_defects(latt, ax, None)
#    axtemp = fig.add_axes()
#    srs = Slider(axtemp, 'Temperature', 0, 5, valinit=1)
#    srs.on_changed(change_temp)
    return fig, ax, ln, ax.quiver(u, v)

def dosteps(framenum, beta, nsteps, ax, ln, quiver):
    """Do some steps, and update the plot"""
    global latt
    for step in range(nsteps):
        latt = mcstep(latt, beta)
    u = np.cos(latt)
    v = np.sin(latt)
    quiver.set_UVC(u, v)
    ax.set_title("MC step #%d" % (framenum * nsteps))
    plot_defects(latt, ax, ln)
    return quiver

pause = False
def run(conf):
    """Run the simulation"""
    global beta
    sz = conf['size']
    beta = 1/max(conf['temp'], 1e-5) # Set kB = 1
    print("beta is now ", beta)
    global latt
    if conf['start'] == 'random':
        latt = 2 * pi * np.random.rand(sz, sz)
    else:
        latt = np.zeros((sz, sz))
    fig, ax, ln, q = doplot_start(latt)

    def onClick(event):
        global pause
        pause ^= True
        if pause:
            a.event_source.stop()
        else:
            a.event_source.start()

    fig.canvas.mpl_connect('button_press_event', onClick)
    a = anim.FuncAnimation(fig, dosteps, fargs=(beta, conf['nsteps'], ax, ln, q), blit=False)
    plt.show()
#    for step in range(conf['nsteps']):
#        dosteps(latt, beta, 1000, q)
#        if step % 1000 == 0:
#            doplot(latt)
#            w = winding_number(latt)
#            #print(w)
#            print("Number of defects: ", np.sum(np.abs(w) >= 2 * pi))
#            print("Step fff ", step)
    
def main():
    """Main entry point"""
    conf = { 'nsteps': 1,
             'size': 32,
             'noise': 3.0,
             'dt': 0.05,
             'temp': 0.7,
             'start': 'zero'}
    run(conf)

if __name__ == '__main__':
    main()
