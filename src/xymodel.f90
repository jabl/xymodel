!****h* /xymodel
! COPYRIGHT
!  Copyright (c) 2006 Janne Blomqvist

!  This program is free software; you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation; either version 2 of the License, or
!  (at your option) any later version.

!  This program is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.

!  You should have received a copy of the GNU General Public License
!  along with this program; if not, write to the Free Software
!  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

! PURPOSE
! Monte Carlo simulation of the 2D XY model. See PRE 47, 1525 and PRE 49, 4250.
!****
program xymodel
  use conf
  use mathconst

  implicit none

  ! Parameters read from the config file.
  real(wp) :: &
       dt = 0.05_wp, & ! Timestep
       noise = 1.0_wp, & ! Noise parameter
       defect_mult = 1.05_wp, & ! How often to output defect 
                                ! count (logarithmically).
       schlieren_mult = 2.0_wp  ! How often to output schlieren pattern.
  integer :: &
       nstep = 3000, & ! Number of steps to run.
       lsz = 512, & ! Size of the lattice is lsz x lsz
       schlieren = 100 ! Output first schlieren pattern at step.
  logical :: &
       benchmark = .false., & ! Run benchmark and test consistency.
       cont_sim = .false. ! Continue a previous simulation.

  ! Work arrays and local data.
  real(wp), allocatable :: latt(:,:), lang_noise(:,:), latt2(:,:)
  integer :: initial_step = 0, &
       coord(2,2)  ! Store coordinates for defect annihilation analysis.
  integer, allocatable :: ind(:)

  ! For seeding PRNG
  integer :: seedsize, ii
  integer, allocatable :: seed(:)

  call read_config ()

  ! See PRNG
  call random_seed(size=seedsize)
  allocate(seed(seedsize))
  seed = (/(ii, ii=1,seedsize*4, 4)/)
  call random_seed(put=seed)

  if (cont_sim) then
     call read_data (initial_step)
  else
     call init_system ()
  end if
  call init_index_vector ()
  if (benchmark) then
     call run_bench ()
  else
     call run (initial_step + 1)
  end if
  ! Write final data.
  call write_data (nstep)
  call write_defects (nstep)
  call write_schlieren (nstep)

contains
  
  !****f* xymodel/read_config
  ! PURPOSE
  ! Read config data with namelist.
  !****
  subroutine read_config ()
    logical :: conf_exist
    character(len=*), parameter :: conffile = 'xyconfig.txt'

    namelist /xyconfig/ nstep, lsz, dt, noise, schlieren, schlieren_mult, &
         defect_mult, benchmark, cont_sim
    inquire (file=conffile, exist=conf_exist)
    if (.not. conf_exist) then
       print *, 'Config file not found. Writing default config file to ', &
            conffile, ' and continuing with default values.'
       open (10, file=conffile, form='formatted', status='new', action='write')
       write (10, xyconfig)
       close (10)
    end if
    open (10, file='xyconfig.txt', form='formatted', status='old', action='read')
    read (10, xyconfig)
    close (10)
    if (verb > 5 .and. .not. cont_sim) then
       write (*, *) 'Current configuration is: '
       write (*, xyconfig)
    end if
  end subroutine read_config

  
  !****f* xymodel/init_system
  ! PURPOSE
  ! Initialize system
  !****
  subroutine init_system ()

    allocate (latt(lsz, lsz), lang_noise(lsz,lsz))

    ! Initial configuration is uniformly distributed between 0 and 2 Pi.
    call random_number (latt)
    latt = latt * 2.0_wp * pi

  end subroutine init_system


  !****f* xymodel/init_index_vector
  ! PURPOSE
  ! Initialize the index vector.
  !****
  subroutine init_index_vector ()
    integer :: i

    if (.not. allocated (ind)) then
       allocate (ind(0:lsz+1))
       ind(0) = lsz
       forall (i = 1:lsz)
          ind(i) = i
       end forall
       ind(lsz+1) = 1
    end if
  end subroutine init_index_vector

  
  !****f* xymodel/run
  ! PURPOSE
  ! Run the main simulation.
  !****
  subroutine run (initial_step)
    integer, intent(in) :: initial_step
    integer :: i, prevs = 1, prevd = 1
    real(wp) :: t1, t2, t3

    if (initial_step == 1) then
       call write_defects (0)
       call write_schlieren (0)
    end if
    call cpu_time (t1)
    call create_noise ()
    call mcstep_ind (initial_step)
    call cpu_time (t2)
    if (verb > 5) then
       print *, 'First mcstep took ', (t2 - t1), ' seconds.'
    end if
    write (*,*) 'Estimated time to finish is: ', 1.16_wp * (t2 - t1) * nstep, ' seconds.'
    write (*,*)
    do i = initial_step + 1, nstep
       if (verb > 11) then
          print *, 'Step ', i
       end if
       call create_noise ()
       call mcstep_ind (i)
       if ((i - prevs + 1) >= schlieren .and. mod (i, 2) == 0 .and. &
            (real (i, wp) / real (prevs, wp) >= schlieren_mult)) then
          if (verb > 10) then
             print *, 'Writing schlieren pattern at step ', i
          end if
          call write_schlieren (i)
!          schlieren = schlieren * schlieren_mult
          prevs = i
       end if
       if ((i - prevd) > 0 .and. mod (i, 2) == 0 .and. &
            (real (i, wp) / real (prevd, wp) >= defect_mult)) then
          if (verb > 10) then
             print *, 'Writing defect count at step ', i
          end if
          call write_defects (i)
          prevd = i
       end if
       ! Write data every 100 steps.
       if (mod (i, 100) == 0) then
          call write_data (i)
       end if
    end do
    call cpu_time (t3)
    print *, 'Total time was ', t3 - t1, ' seconds.'
  end subroutine run


  !****f* xymodel/run_bench
  ! PURPOSE
  ! Run benchmark comparing speed of mcstep_loop and mcstep_cshift.
  ! Also verifies that the output using both methods are identical.
  !****
  subroutine run_bench ()
    real(wp) :: t1, t2, t3
    integer :: i, j
    real(wp), allocatable :: origlatt(:,:), tmplatt(:,:)

    allocate (origlatt(lsz, lsz), tmplatt(lsz, lsz))
    origlatt = latt
    print *, 'Running mc step benchmark.'
    call cpu_time (t1)
    call mcstep_loop (1)
    call mcstep_loop (2)
    call cpu_time (t2)
    t3 = t2 - t1
    print *, 'Time for one mc step using explicit loops was ', t3 / real (2, wp), ' seconds.'
    tmplatt = latt
    latt = origlatt
    call cpu_time (t1)
    call mcstep_cshift ()
    call mcstep_cshift ()
    call cpu_time (t2)
    print *, 'Time for one mc step using cshift was ', (t2 - t1) / real (2, wp), &
         ' seconds.'
    print *, 'Abstraction overhead factor: ', (t2 - t1) / t3
    ! Check results.
    do j = 1, lsz
       do i = 1, lsz
          if ((latt(i,j) - tmplatt(i,j)) > sqrt (epsilon (1.0_wp))) then
             print *, "Loop vs. cshift results don't match for lattice site ", i, j
             print *, 'Difference: ', latt(i,j)-tmplatt(i,j)
          end if
       end do
    end do
    latt = origlatt
    call cpu_time (t1)
    call mcstep_ind (1)
    call mcstep_ind (2)
    call cpu_time (t2)
    print *, 'Time for one mc step using index vector was ', (t2 - t1) / real (2, wp), &
         ' seconds.'
    print *, 'Abstraction overhead factor: ', (t2 - t1)/t3
    ! Check
    do j = 1, lsz
       do i = 1, lsz
          if ((latt(i,j) - tmplatt(i,j)) > sqrt (epsilon (1.0_wp))) then
             print *, "Loop vs. index vector results don't match for lattice site ", i, j
          end if
       end do
    end do

  end subroutine run_bench


  !****f* xymodel/create_noise
  ! PURPOSE
  ! Fill the noise array.
  !****
  subroutine create_noise ()
    call random_number (lang_noise)
    lang_noise = lang_noise - 0.5_wp
    lang_noise = lang_noise * 2.0_wp * noise * pi
  end subroutine create_noise


  !****f* xymodel/mcstep_loop
  ! PURPOSE
  ! Do a single Monte Carlo step using explicit loops.
  !****
  subroutine mcstep_loop (loopid)
    integer, intent(in) :: loopid
    integer :: i, j

    ! To preserve detailed balance we cannot update the array we are
    ! looping over, and since we include the diagonal neighbors one cannot
    ! use the checkerboard update either. So we switch between two arrays.

    if (.not. allocated (latt2)) then
       allocate (latt2(lsz, lsz))
    end if

    if (mod (loopid, 2) == 1) then  ! Read from latt, store into latt2

       ! First do the interior points.
       do j = 2, lsz-1
          do i = 2, lsz -1 
             latt2(i,j) = latt(i,j) - dt * (lang_noise(i,j) + &
                  ! N, E, S, W nearest neighbors.
                  sin (latt(i,j) - latt(i, j+1)) + sin (latt(i,j) - latt(i+1, j)) + &
                  sin (latt(i,j) - latt(i, j-1)) + sin (latt(i,j) - latt(i-1, j)) + &
                  ! NE, SE, SW, NW
                  sin (latt(i,j) - latt(i+1, j+1)) + sin (latt(i,j) - latt(i+1, j-1)) + &
                  sin (latt(i,j) - latt(i-1, j-1)) + sin (latt(i,j) - latt(i-1, j+1)) )
          end do
       end do

       ! Then the edges taking care of the PBC.
       do i = 2, lsz-1
          ! North edge (i is x coord).
          latt2(i, lsz) = latt(i, lsz) - dt * (lang_noise (i, lsz) + &
               sin (latt(i,lsz) - latt(i,1)) + sin (latt(i,lsz) - latt(i+1,lsz))+ &
               sin (latt(i,lsz) - latt(i,lsz-1)) + sin (latt(i,lsz) - latt(i-1,lsz)) + &
               sin (latt(i,lsz) - latt(i+1,1)) + sin (latt(i,lsz) - latt(i+1,lsz-1)) + &
               sin (latt(i,lsz) - latt(i-1,lsz-1)) + sin (latt(i,lsz) - latt(i-1,1)))
          ! East edge (i is y coord).
          latt2(lsz, i) = latt(lsz, i) - dt * (lang_noise (lsz, i) + &
               sin (latt(lsz,i) - latt(lsz,i+1)) + sin (latt(lsz,i) - latt(1,i)) + &
               sin (latt(lsz,i) - latt(lsz,i-1)) + sin (latt(lsz,i) - latt(lsz-1,i)) + &
               sin (latt(lsz,i) - latt(1,i+1)) + sin (latt(lsz,i) - latt(1,i-1)) + &
               sin (latt(lsz,i) - latt(lsz-1,i-1)) + sin (latt(lsz,i) - latt(lsz-1,i+1)))
          ! South edge (i is x coord).
          latt2(i, 1) = latt(i, 1) - dt * (lang_noise (i, 1) + &
               sin (latt(i,1) - latt(i,2)) + sin (latt(i,1) - latt(i+1,1))+ &
               sin (latt(i,1) - latt(i,lsz)) + sin (latt(i,1) - latt(i-1,1)) + &
               sin (latt(i,1) - latt(i+1,2)) + sin (latt(i,1) - latt(i+1,lsz)) + &
               sin (latt(i,1) - latt(i-1,lsz)) + sin (latt(i,1) - latt(i-1,2)))
          ! West edge (i is y coord).
          latt2(1, i) = latt(1, i) - dt * (lang_noise (1, i) + &
               sin (latt(1,i) - latt(1,i+1)) + sin (latt(1,i) - latt(2,i)) + &
               sin (latt(1,i) - latt(1,i-1)) + sin (latt(1,i) - latt(lsz,i)) + &
               sin (latt(1,i) - latt(2,i+1)) + sin (latt(1,i) - latt(2,i-1)) + &
               sin (latt(1,i) - latt(lsz,i-1)) + sin (latt(1,i) - latt(lsz,i+1)))
       end do

       ! Finally, the corners.
       ! NE
       latt2(lsz,lsz) = latt(lsz,lsz) - dt * (lang_noise(lsz, lsz) + &
            sin (latt(lsz,lsz) - latt(lsz,1)) + sin (latt(lsz,lsz) - latt(1,lsz)) + &
            sin (latt(lsz,lsz) - latt(lsz,lsz-1)) + sin (latt(lsz,lsz) - latt(lsz-1,lsz)) + &
            sin (latt(lsz,lsz) - latt(1,1)) + sin (latt(lsz,lsz) - latt(1,lsz-1)) + &
            sin (latt(lsz,lsz) - latt(lsz-1,lsz-1)) + sin (latt(lsz,lsz) - latt(lsz-1,1)) )
       ! SE
       latt2(lsz,1) = latt(lsz,1) - dt * (lang_noise(lsz,1) + &
            sin (latt(lsz,1) - latt(lsz,2)) + sin (latt(lsz,1) - latt(1,1))+ &
            sin (latt(lsz,1) - latt(lsz,lsz)) + sin (latt(lsz,1) - latt(lsz-1,1))+ &
            sin (latt(lsz,1) - latt(1,2)) + sin (latt(lsz,1) - latt(1,lsz))+ &
            sin (latt(lsz,1) - latt(lsz-1,lsz)) + sin (latt(lsz,1) - latt(lsz-1,2)))
       ! SW
       latt2(1,1) = latt(1,1) - dt * (lang_noise(1,1) + &
            sin (latt(1,1) - latt(1,2)) + sin (latt(1,1) - latt(2,1)) + &
            sin (latt(1,1) - latt(1,lsz)) + sin (latt(1,1) - latt(lsz,1)) + &
            sin (latt(1,1) - latt(2,2)) + sin (latt(1,1) - latt(2,lsz)) + &
            sin (latt(1,1) - latt(lsz,lsz)) + sin (latt(1,1) - latt(lsz,2)))
       ! NW
       latt2(1,lsz) = latt(1,lsz) - dt * (lang_noise(1,lsz) + &
            sin (latt(1,lsz) - latt(1,1)) + sin (latt(1,lsz) - latt(2,lsz)) + &
            sin (latt(1,lsz) - latt(1,lsz-1)) + sin (latt(1,lsz) - latt(lsz,lsz)) + &
            sin (latt(1,lsz) - latt(2,1)) + sin (latt(1,lsz) - latt(2,lsz-1)) + &
            sin (latt(1,lsz) - latt(lsz,lsz-1)) + sin (latt(1,lsz) - latt(lsz,1)) )

    else  ! Read from latt2, store into latt1

       ! First do the interior points.
       do j = 2, lsz-1
          do i = 2, lsz -1 
             latt(i,j) = latt2(i,j) - dt * (lang_noise(i,j) + &
                  ! N, E, S, W nearest neighbors.
                  sin (latt2(i,j) - latt2(i, j+1)) + sin (latt2(i,j) - latt2(i+1, j)) + &
                  sin (latt2(i,j) - latt2(i, j-1)) + sin (latt2(i,j) - latt2(i-1, j)) + &
                  ! NE, SE, SW, NW
                  sin (latt2(i,j) - latt2(i+1, j+1)) + sin (latt2(i,j) - latt2(i+1, j-1)) + &
                  sin (latt2(i,j) - latt2(i-1, j-1)) + sin (latt2(i,j) - latt2(i-1, j+1)) )
          end do
       end do

       ! Then the edges taking care of the PBC.
       do i = 2, lsz-1
          ! North edge (i is x coord).
          latt(i, lsz) = latt2(i, lsz) - dt * (lang_noise (i, lsz) + &
               sin (latt2(i,lsz) - latt2(i,1)) +      sin (latt2(i,lsz) - latt2(i+1,lsz))+ &
               sin (latt2(i,lsz) - latt2(i,lsz-1)) + sin (latt2(i,lsz) - latt2(i-1,lsz)) + &
               sin (latt2(i,lsz) - latt2(i+1,1)) +    sin (latt2(i,lsz) - latt2(i+1,lsz-1)) + &
               sin (latt2(i,lsz) - latt2(i-1,lsz-1)) + sin (latt2(i,lsz) - latt2(i-1,1)))
          ! East edge (i is y coord).
          latt(lsz, i) = latt2(lsz, i) - dt * (lang_noise (lsz, i) + &
               sin (latt2(lsz,i) - latt2(lsz,i+1)) + sin (latt2(lsz,i) - latt2(1,i)) + &
               sin (latt2(lsz,i) - latt2(lsz,i-1)) + sin (latt2(lsz,i) - latt2(lsz-1,i)) + &
               sin (latt2(lsz,i) - latt2(1,i+1)) +    sin (latt2(lsz,i) - latt2(1,i-1)) + &
               sin (latt2(lsz,i) - latt2(lsz-1,i-1)) + sin (latt2(lsz,i) - latt2(lsz-1,i+1)))
          ! South edge (i is x coord).
          latt(i, 1) = latt2(i, 1) - dt * (lang_noise (i, 1) + &
               sin (latt2(i,1) - latt2(i,2)) +      sin (latt2(i,1) - latt2(i+1,1))+ &
               sin (latt2(i,1) - latt2(i,lsz)) +   sin (latt2(i,1) - latt2(i-1,1)) + &
               sin (latt2(i,1) - latt2(i+1,2)) +    sin (latt2(i,1) - latt2(i+1,lsz)) + &
               sin (latt2(i,1) - latt2(i-1,lsz)) + sin (latt2(i,1) - latt2(i-1,2)))
          ! West edge (i is y coord).
          latt(1, i) = latt2(1, i) - dt * (lang_noise (1, i) + &
               sin (latt2(1,i) - latt2(1,i+1)) + sin (latt2(1,i) - latt2(2,i)) + &
               sin (latt2(1,i) - latt2(1,i-1)) + sin (latt2(1,i) - latt2(lsz,i)) + &
               sin (latt2(1,i) - latt2(2,i+1)) + sin (latt2(1,i) - latt2(2,i-1)) + &
               sin (latt2(1,i) - latt2(lsz,i-1)) + sin (latt2(1,i) - latt2(lsz,i+1)))
       end do

       ! Finally, the corners.
       ! NE
       latt(lsz,lsz) = latt2(lsz,lsz) - dt * (lang_noise(lsz, lsz) + &
            sin (latt2(lsz,lsz) - latt2(lsz,1)) + sin (latt2(lsz,lsz) - latt2(1,lsz)) + &
            sin (latt2(lsz,lsz) - latt2(lsz,lsz-1)) + sin (latt2(lsz,lsz) - latt2(lsz-1,lsz)) + &
            sin (latt2(lsz,lsz) - latt2(1,1)) + sin (latt2(lsz,lsz) - latt2(1,lsz-1)) + &
            sin (latt2(lsz,lsz) - latt2(lsz-1,lsz-1)) + sin (latt2(lsz,lsz) - latt2(lsz-1,1)) )
       ! SE
       latt(lsz,1) = latt2(lsz,1) - dt * (lang_noise(lsz,1) + &
            sin (latt2(lsz,1) - latt2(lsz,2)) + sin (latt2(lsz,1) - latt2(1,1))+ &
            sin (latt2(lsz,1) - latt2(lsz,lsz)) + sin (latt2(lsz,1) - latt2(lsz-1,1))+ &
            sin (latt2(lsz,1) - latt2(1,2)) + sin (latt2(lsz,1) - latt2(1,lsz))+ &
            sin (latt2(lsz,1) - latt2(lsz-1,lsz)) + sin (latt2(lsz,1) - latt2(lsz-1,2)))
       ! SW
       latt(1,1) = latt2(1,1) - dt * (lang_noise(1,1) + &
            sin (latt2(1,1) - latt2(1,2)) + sin (latt2(1,1) - latt2(2,1)) + &
            sin (latt2(1,1) - latt2(1,lsz)) + sin (latt2(1,1) - latt2(lsz,1)) + &
            sin (latt2(1,1) - latt2(2,2)) + sin (latt2(1,1) - latt2(2,lsz)) + &
            sin (latt2(1,1) - latt2(lsz,lsz)) + sin (latt2(1,1) - latt2(lsz,2)))
       ! NW
       latt(1,lsz) = latt2(1,lsz) - dt * (lang_noise(1,lsz) + &
            sin (latt2(1,lsz) - latt2(1,1)) + sin (latt2(1,lsz) - latt2(2,lsz)) + &
            sin (latt2(1,lsz) - latt2(1,lsz-1)) + sin (latt2(1,lsz) - latt2(lsz,lsz)) + &
            sin (latt2(1,lsz) - latt2(2,1)) + sin (latt2(1,lsz) - latt2(2,lsz-1)) + &
            sin (latt2(1,lsz) - latt2(lsz,lsz-1)) + sin (latt2(1,lsz) - latt2(lsz,1)) )

    end if
  end subroutine mcstep_loop


  !****f* xymodel/mcstep_ind
  ! PURPOSE
  ! Do a single Monte Carlo step using an index vector to avoid repeated mod 
  ! calculations.
  !****
  subroutine mcstep_ind (step)
    integer, intent(in) :: step
    integer :: i, j

    ! To preserve detailed balance we cannot update the array we are
    ! looping over, and since we include the diagonal neighbors one cannot
    ! use the checkerboard update either. So we switch between two arrays.

    if (.not. allocated (latt2)) then
       allocate (latt2(lsz, lsz))
    end if

    if (mod (step, 2) == 1) then  ! Read from latt, store into latt2
       do j = 1, lsz
          do i = 1, lsz
             latt2(i,j) = latt(i,j) - dt * (lang_noise(i,j) + &
                  ! N, E, S, W nearest neighbors.
                  sin (latt(i,j) - latt(i, ind(j+1))) + sin (latt(i,j) - latt(ind(i+1), j)) + &
                  sin (latt(i,j) - latt(i, ind(j-1))) + sin (latt(i,j) - latt(ind(i-1), j)) + &
                  ! NE, SE, SW, NW
                  sin (latt(i,j) - latt(ind(i+1), ind(j+1))) + sin (latt(i,j) - latt(ind(i+1), ind(j-1))) + &
                  sin (latt(i,j) - latt(ind(i-1), ind(j-1))) + sin (latt(i,j) - latt(ind(i-1), ind(j+1))) )
          end do
       end do
    else
       do j = 1, lsz
          do i = 1, lsz
             latt(i,j) = latt2(i,j) - dt * (lang_noise(i,j) + &
                  ! N, E, S, W nearest neighbors.
                  sin (latt2(i,j) - latt2(i, ind(j+1))) + sin (latt2(i,j) - latt2(ind(i+1), j)) + &
                  sin (latt2(i,j) - latt2(i, ind(j-1))) + sin (latt2(i,j) - latt2(ind(i-1), j)) + &
                  ! NE, SE, SW, NW
                  sin (latt2(i,j) - latt2(ind(i+1), ind(j+1))) + sin (latt2(i,j) - latt2(ind(i+1), ind(j-1))) + &
                  sin (latt2(i,j) - latt2(ind(i-1), ind(j-1))) + sin (latt2(i,j) - latt2(ind(i-1), ind(j+1))) )
          end do
       end do
    end if
  end subroutine mcstep_ind


  !****f* xymodel/mcstep_cshift
  ! PURPOSE
  ! Do a single Monte Carlo step using the cshift intrinsic.
  !****
  subroutine mcstep_cshift ()
    latt = latt - dt * (lang_noise + &
         ! 4 nearest neighbors N, S, E, W
         sin (latt - cshift (latt, -1, 1)) + &
         sin (latt - cshift (latt, -1, 2)) + &
         sin (latt - cshift (latt, 1, 1)) + &
         sin (latt - cshift (latt, 1, 2)) + &
         ! 4 nearest neighbors NE, SE, SW, NW
         sin (latt - cshift (cshift (latt, -1, 1), -1, 2)) + &
         sin (latt - cshift (cshift (latt, -1, 1), 1, 2)) + &
         sin (latt - cshift (cshift (latt, 1, 1), -1, 2)) + &
         sin (latt - cshift (cshift (latt, 1, 1), 1, 2)) )
  end subroutine mcstep_cshift


  !****f* xymodel/write_defects
  ! PURPOSE
  ! Write the number of defects and the
  ! iteration number to file.
  !****
  subroutine write_defects (step)
    integer, intent(in) :: step
    character(len=30), parameter :: filename = "defects.dat"
    logical :: fexist, created = .false., reduced_mult = .false.
    integer :: ndefects !, ndefects_energy
    real(wp) :: t1, t2

    inquire (file=filename, exist=fexist)
    if (.not. fexist .or. (.not. cont_sim .and. .not. created)) then
       open (11, file=filename, form='formatted', status='replace', action='write', &
            position='rewind')
       write (11, '(a)') '# Iterations    Defects'
       created = .true.
    else
       open (11, file=filename, form='formatted', status='old', action='write', &
            position='append')
    end if
    call cpu_time (t1)
    ndefects = count_defects_winding ()
    call cpu_time (t2)
    if (verb > 11) then
       print *, 'Time to count defects using winding number: ', t2-t1
    end if
!    call cpu_time (t1)
!    ndefects_energy = count_defects ()
!    call cpu_time (t2)
!    if (verb > 11) then
!       print *, 'Time to count defects using energy: ', t2-t1
!    end if
    write (11, '(1x,i6,8x,i6,2x,i6)') step, ndefects
    close (11)
    if (ndefects < 5 .and. (.not. reduced_mult)) then
       ! Write out defect numbers and distance more often
       defect_mult = 1.0_wp + (defect_mult - 1.0_wp) / real (100, wp)
       print *, 'new defect mult is ', defect_mult
       reduced_mult = .true.
    end if
    if (ndefects == 2) then
       call write_defect_distance (step)
    end if
    if (ndefects <= 1) then
       stop 'All defects but one annihilated, stopping'
    end if
  end subroutine write_defects


  !****f* xymodel/write_defect_distance
  ! PURPOSE
  ! Write distance between two defects.
  !****
  subroutine write_defect_distance (step)
    integer, intent(in) :: step
    character(len=30) :: filename = 'defect_distance.dat'
!    integer :: i
    logical :: fexist, created = .false.
    real(wp) :: dist, vdist(2)

    ! Calculate distance, taking into account periodic boundary conditions.
    vdist = real (coord(1,:), wp) - real (coord(2,:), wp)
    vdist = abs (vdist) / real (lsz, wp)
    vdist = vdist - anint (vdist)
    vdist = vdist * real (lsz, wp)
    dist = sqrt (dot_product (vdist, vdist))

    inquire (file='defect_distance.dat', exist=fexist)
    if (.not. fexist .or. .not. created) then
       open (14, file=filename, form='formatted', status='replace', &
            action='write', &
            position='rewind')
       write (14, '(a)') '# Iterations    Defect distance'
       created = .true.
    else
       open (14, file=filename, form='formatted', status='old', action='write', &
            position='append')
    end if
    write (14, '(1x,i6,8x,f9.3)') step, dist
    close (14)
  end subroutine write_defect_distance


  !****f* xymodel/count_defects
  ! PURPOSE
  ! Count the number of defect cores in the lattice.
  ! This implementation is horribly inefficient, but if it isn't
  ! called too often it doesn't matter.
  !****
  function count_defects () result (def)
    integer :: def, i, j
    real(wp), allocatable, save :: energy(:,:)
    logical, allocatable, save :: locmax(:,:)

    def = 0

    if (.not. allocated (energy)) then
       allocate (energy (lsz, lsz), locmax(lsz, lsz))
    end if

    energy = 8.0_wp - &
         ! 4 nearest neighbors N, E, S, W
         cos (latt - cshift (latt, -1, 1)) - &
         cos (latt - cshift (latt, -1, 2)) - &
         cos (latt - cshift (latt, 1, 1)) - &
         cos (latt - cshift (latt, 1, 2)) - &
         ! 4 nearest neighbors NE, SE, SW, NW
         cos (latt - cshift (cshift (latt, -1, 1), -1, 2)) - &
         cos (latt - cshift (cshift (latt, -1, 1), 1, 2)) - &
         cos (latt - cshift (cshift (latt, 1, 1), -1, 2)) - &
         cos (latt - cshift (cshift (latt, 1, 1), 1, 2))

    locmax = (latt > cshift (latt, -1, 1)) .and. &
         (latt > cshift (latt, -1, 2)) .and. &
         (latt > cshift (latt, 1, 1)) .and. &
         (latt > cshift (latt, 1, 2)) .and. &
         ! 4 nearest neighbors NE, SE, SW, NW
         (latt > cshift (cshift (latt, -1, 1), -1, 2)) .and. &
         (latt > cshift (cshift (latt, -1, 1), 1, 2)) .and. &
         (latt > cshift (cshift (latt, 1, 1), -1, 2)) .and. &
         (latt > cshift (cshift (latt, 1, 1), 1, 2))


    do j = 1, lsz
       do i = 1, lsz
          if (energy(i,j) > 5.0_wp .and. locmax(i,j)) then
             def = def + 1
             ! Store last two defect coordinates in coord
!             coord (mod (def, 2) + 1, 1) = i
!             coord (mod (def, 2) + 1, 2) = j
          end if
       end do
    end do

  end function count_defects


  !****f* xymodel/count_defects_wind
  ! PURPOSE
  ! Count the defect cores using the winding number.
  !****
  function count_defects_winding () result (def)
    integer :: def, i, j

    def = 0

    do j = 1, lsz
       do i = 1, lsz
          ! Going through the 4 corners counterclockwise, check if
          ! (2-1) + (3-2) + (4-3) + (1-4) > +- 2*pi. Corners are 
          ! identified by the indexes:
          ! 1: i+1, j+1
          ! 2: i, j+1
          ! 3: i, j
          ! 4: i+1, j
          if (abs (norm_angle (norm_angle (latt(i, ind(j+1))) &
               - norm_angle (latt(ind(i+1), ind(j+1)))) &
               + norm_angle (norm_angle (latt(i,j)) &
               - norm_angle (latt(i, ind(j+1)))) &
               + norm_angle (norm_angle (latt(ind(i+1), j)) &
               - norm_angle (latt(i, j))) &
               + norm_angle (norm_angle (latt(ind(i+1), ind(j+1))) &
               - norm_angle (latt(ind(i+1), j)))) &
               > 2.0_wp * pi - epsilon (1.0_wp)) then
             def = def + 1
             ! Store last two defect coordinates in coord
             coord (mod (def, 2) + 1, 1) = i
             coord (mod (def, 2) + 1, 2) = j
          end if
       end do
    end do
  end function count_defects_winding


  !****f* xymodel/norm_angle
  ! PURPOSE
  ! Normalize an angle so that it is (-pi, pi].
  !****
  function norm_angle (angle)
    real(wp), intent(in) :: angle
    real(wp) :: norm_angle

    norm_angle = modulo (angle, 2.0_wp * pi)
    if (norm_angle > pi) then
       norm_angle = - (2.0_wp * pi - norm_angle)
    end if
  end function norm_angle


  !****f* xymodel/write_schlieren
  ! PURPOSE
  ! Write Schlieren pattern to a text file, so one can plot 
  ! it e.g. with octave.
  !****
  subroutine write_schlieren (step)
    integer, intent(in) :: step
    character(len=30) :: filename
    integer :: i

    write (filename, '(A,i0,a)') 'schlieren_', step, '.dat'
    open (10, file=filename, form='formatted', action='write')
    do i = 1, lsz
       write (10, '(9999(1X,f6.4))') sin(2.0_wp * latt(:,i))**2
    end do
    close (10)
  end subroutine write_schlieren


  !***f* xymodel/write_data
  ! PURPOSE
  ! Write the data to file so that the simulation can be continued.
  !****
  subroutine write_data (step)
    integer, intent(in) :: step

    open (12, file="xy.dat", access="sequential", form="unformatted", &
         action="write", status="replace", position="rewind")
    write (12) lsz, step
    write (12) latt
    close (12)
  end subroutine write_data


  !***f* xymodel/read_data
  ! PURPOSE
  ! Read data from a previous simulation.
  !****
  subroutine read_data (step)
    integer, intent(out) :: step

    open (13, file='xy.dat', access='sequential', form='unformatted', &
         action='read', status='old', position='rewind')
    read (13) lsz, step
    allocate (latt(lsz, lsz), lang_noise (lsz, lsz))
    print *, 'Continuing simulation from step ', step
    read (13) latt
    close (13)
  end subroutine read_data

end program xymodel
