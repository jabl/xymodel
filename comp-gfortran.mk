FC := gfortran

OFLAG := -O2 -g -ffast-math -ftree-vectorize -funroll-loops -march=native
DEBUG := -O1 -g -fcheck=all
#LIBS := -llapackmt -lblasmt
#LDFLAGS := --staticlink
FFLAGS := -std=f95 -pedantic -Wall
MODDIR := -I $(SRCDIR) -J $(SRCDIR)
