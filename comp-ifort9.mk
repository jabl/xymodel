FC := ifort

OFLAG := -O2
DEBUG := -g -C
LDFLAGS := -L/usr/lib/sse2
LIBS := -llapack -lf77blas -latlas
FFLAGS := $(DEBUG) -warn all -fpscomp none -std95
MODDIR := -module $(SRCDIR) -i $(SRCDIR)
