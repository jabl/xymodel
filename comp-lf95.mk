FC := lf95

OFLAG := --o2 --ntrace --nap --nchk --ng --nsav --prefetch 2 --tpp
DEBUG := -g --ap --chkglobal --lst --trace --pca --xref --trap
#LIBS := -llapackmt -lblasmt
LDFLAGS := --staticlink
FFLAGS := --f95 --wo --warn
MODDIR := -M $(SRCDIR)
